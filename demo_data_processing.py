# -*- coding: utf-8 -*-
"""
Created on Wed Mar 10 13:15:34 2021

@author: g.vitale
"""


from random import randint as ri
from random import uniform as ru
from random import sample as rc

import numpy as np
import cv2 as cv
from sklearn.decomposition import PCA

import matplotlib.pyplot as plt
import matplotlib.colors as mcolors
from mpl_toolkits.mplot3d import Axes3D

import time

from datasetArca_v01 import DatasetARCA



# data_root = 'C:/Users/g.vitale/Desktop/myStuff/ARCA/sample_data'
# dataset_name = 'RS32H0000002'
# label = 'False'
# ds = DatasetARCA(data_root=data_root, name=dataset_name, label=label, 
#                  is_gt=True, options=options)



" user input "
data_path_1 = 'C:/Users/g.vitale/Desktop/myStuff/ARCA/sample_data/RS32F0000002/True'
data_path_2 = 'C:/Users/g.vitale/Desktop/myStuff/ARCA/sample_data/RS32F0000181/False'
data_path_list = [data_path_1, data_path_2]

" dataset "
options={'sensor'        : ['0'],
         'currency'      : ['eu'],
         'value'         : None, #['j', 'e'],
         'emission'      : None, #['a'],
         'orientation'   : None, #['1'],
         'out_stats'     : True,
                               }
ds = DatasetARCA(data_path_list=data_path_list, is_gt=True, options=options)
dsi = iter(ds)

class_nms = ['value']
keys = list(ds.db.groupby(class_nms).groups.keys())
class_ids = dict(zip(keys, range(len(keys))))
# class_col = [ (ru(0,1),ru(0,1),ru(0,1)) for k in keys]
class_col = rc(list(mcolors.TABLEAU_COLORS.values()), len(keys))


# " data processing "
# item = next(dsi)
# img = item['data']['rf']
# print(item['info'])
# # img = cv.equalizeHist(img)
# # cv.imshow('rf',img)
# # cv.waitKey(0)  
# # cv.destroyAllWindows() 
# # _, imt = cv.threshold(img, 0,255, cv.THRESH_BINARY+cv.THRESH_OTSU)
# # cv.imshow('thr',imt)
# # cv.waitKey(0)  
# # cv.destroyAllWindows() 

# bins = range(255)
# hst = np.histogram(img[0,:], bins=bins)
# plt.hist(img[0,:], bins=bins)

# np.mean(img[0,:]), np.std(img[0,:])
# np.mean(img[10,:]), np.std(img[10,:])
# np.mean(img[100,:]), np.std(img[100,:])



" ----- process data -------------------------------------------------------- "

winSize =  (64,64) #(64,128)
blockSize = (32,32) #(16,16)
blockStride = (32,32) #(8,8)
cellSize = (32,32) #(16,16) #(8,8)
nbins = 7
derivAperture = 1
winSigma = 4.
histogramNormType = 0
L2HysThreshold = 2.0e-01
gammaCorrection = 0
nlevels = 64

hog = cv.HOGDescriptor(winSize, blockSize, blockStride, 
                        cellSize, nbins, derivAperture, winSigma,
                        histogramNormType, L2HysThreshold, 
                        gammaCorrection, nlevels)

winStride = (32,32) #(8,8)
padding = (8,8)
# locations = ((10,20),)

# hog = cv.HOGDescriptor()
# hog.save("./hog.xml")
feats = []
label = []
p = 20
for n, item in enumerate(dsi):    
    img = item['data']['rf']
    img = img[p:img.shape[0]-p, p:img.shape[1]-p]
    # label.append(item['info']['value'])
    class_id = tuple(item['info'][class_nms])
    class_id = class_id if len(class_id)>1 else class_id[0]
    label.append(class_ids[class_id])
    fea = hog.compute(img, winStride, padding).squeeze()
    # fea = hog.compute(img, winStride, padding, locations).squeeze()
    # fea = hog.compute(img).squeeze()
    feats.append(fea)
feats = np.array(feats)
elapsed = time.time()
pca = PCA(n_components=3)
pca.fit(feats)
elapsed = time.time() - elapsed
feats = pca.transform(feats)
# print('time per feature computation:', elapsed)
print('time per pca clustering:', elapsed)
print('nb dof hog feat: ', fea.size)



" ----- plotting and visualization ------------------------------------------ "

fig = plt.figure(1, figsize=(4, 3))
plt.clf()
ax = Axes3D(fig, rect=[0, 0, .95, 1], elev=48, azim=134)
plt.cla()
col = [class_col[l] for l in label]
ax.scatter(feats[:, 0], feats[:, 1], feats[:, 2], 
           c=col,           
           cmap=plt.cm.nipy_spectral,
           edgecolor='k')
ax.w_xaxis.set_ticklabels([])
ax.w_yaxis.set_ticklabels([])
ax.w_zaxis.set_ticklabels([])
plt.show()
