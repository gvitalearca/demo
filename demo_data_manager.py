#!/usr/bin/env python3

import os
import sys
import glob

import numpy as np
import pandas as pd
import cv2 as cv
import math
import scipy.optimize

from matplotlib import pyplot as plt



" ----- User Defined Paths -------------------------------------------------- "

data_root = 'C:/Users/g.vitale/Desktop/myStuff/ARCA/sample_data'
dataset_name = 'RS32F0000002'
label = 'True'
data_path = os.path.join(data_root, dataset_name, label)

project_root = 'C:/Users/g.vitale/Desktop/myStuff/ARCA/development/demo'
output_root = os.path.join(project_root, 'data')

" --------------------------------------------------------------------------- "



" ----- Open Folders and Read Files ----------------------------------------- "

" acquisitions "
acqs = os.listdir(data_path)
acqs = [a for a in acqs if os.path.isdir(os.path.join(data_path,a))]

" example vis data "
ptr = glob.glob(os.path.join(data_path, acqs[0], '*.bmp'))[0]
img_raw = cv.imread(ptr,0)
print(img_raw.shape)
pxl_ch = img_raw.shape[1]//7
rf = img_raw[:,:pxl_ch]
gf = img_raw[:,pxl_ch:2*pxl_ch]
hf = img_raw[:,2*pxl_ch:3*pxl_ch]
lst = [rf, gf, hf]

# for im in lst:
#     cv.imshow('raw',im)
#     cv.waitKey(0)  
#     cv.destroyAllWindows() 

" example magn data "
ptr = glob.glob(os.path.join(data_path, acqs[1], '*.bmp'))[0]
img_raw = cv.imread(ptr,0)
print(img_raw.shape)
cv.imshow('raw',img_raw)
cv.waitKey(0)  
cv.destroyAllWindows() 

" example thickness data "
ptr = glob.glob(os.path.join(data_path, acqs[2], '*.bmp'))[0]
img_raw = cv.imread(ptr,0)
print(img_raw.shape)
cv.imshow('raw',img_raw)
cv.waitKey(0)  
cv.destroyAllWindows() 
    
" --------------------------------------------------------------------------- "


