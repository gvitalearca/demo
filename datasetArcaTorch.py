# -*- coding: utf-8 -*-
"""
Created on Thu Mar 25 11:31:55 2021

@author: g.vitale
"""

from PIL import Image

import torch
from torch.utils.data import Dataset
import torchvision.transforms as transforms

from datasetArca_v01 import DatasetARCA



class DatasetARCAtorch(Dataset, DatasetARCA):

    def __init__(self, 
                 data_path_list='',
                 is_gt=True,
                 options={'sensor'        : None,
                          'currency'      : None,
                          'value'         : None,
                          'emission'      : None,
                          'orientation'   : None,
                          'out_stats'     : True,
                          }, 
                 transform=None):
        
        " call the parent constructors "
        DatasetARCA.__init__(self, data_path_list, is_gt, options)
        
        " sensor options, optical case "
        if type(self.options['sensor']==tuple) and self.options['sensor'][0]=='0':
            self.opt_chn = self.options['sensor'][1]
            self.options['sensor'] = '0'
            
        " new members "
        self.class_names = None
        self.class2idx = {}
        self.idx2class = {}    
        self.transform = transform
        self.size = (200,200)
        self.aff = 3
        self.deg = 3
        self.arca_std_transf = transforms.Compose([
                    				transforms.RandomCrop(self.size),
                                    transforms.RandomAutocontrast(),
                                    transforms.RandomAffine(self.aff),
                    				transforms.RandomRotation(degrees=self.deg),
                    				transforms.ToTensor()
                                    ])
    
                                        
    " ----- Private Methods ------------------------------------------------- "
    
    " apply torch transformations "
    def _apply_transf(self, data):
        for k in data.keys():
            bmp = data[k]
            if self.transform is not None:            
                if self.transform=='std':
                    data[k] = self.arca_std_transf(bmp)
                else:
                    data[k] = self.transform(bmp)
            else:
                data[k] = transforms.ToTensor()(bmp)             
        return data
            
    " open the bmp data the sensors "
    def _open_dat(self, ptrs):
        item = {}
        for avsns in list(set(self.db['sensor'])):
            ptr = ptrs[ptrs['sensor'] == avsns]['data'].iloc[0]
            item[avsns] = Image.open(ptr)
            if avsns == '0':
                w,h = item[avsns].size
                s = w//7
                item['rf'] = item[avsns].crop(( 0*s, 0, 1*s, h))
                item['gf'] = item[avsns].crop(( 1*s, 0, 2*s, h))
                item['if'] = item[avsns].crop(( 2*s, 0, 3*s, w))
                item['rr'] = item[avsns].crop(( 3*s, 0, 4*s, w))
                item['gr'] = item[avsns].crop(( 4*s, 0, 5*s, w))
                item['ir'] = item[avsns].crop(( 5*s, 0, 6*s, w))
                item['tr'] = item[avsns].crop(( 6*s, 0, 7*s, w))
                item.pop('0')
        item = self._apply_transf(item)                                
        return item


    " ----- Public Methods -------------------------------------------------- "
    
    def set_classes(self, class_names=['value','orientation']):
        self.class_names = class_names
        keys = list(self.db.groupby(self.class_names).groups.keys())
        self.class2idx = dict(zip(keys, range(len(keys))))
        self.idx2class = dict(zip( range(len(keys)), keys))
    
    def __getitem__(self, idx):
        item = DatasetARCA.__getitem__(self, idx)
        data = item['data'][self.opt_chn]
        clss = [item['info'][c] for c in self.class_names]
        target = self.class2idx[tuple(clss) if len(clss)>1 else clss[0]]
        target = torch.LongTensor([target])
        return data, target.squeeze()

    def __len__(self):
        return len(self.db.index)

    # TODO: balance database if asked
    # TODO: database splitting: train, validation, test






# " ----- TEST ---------------------------------------------------------------- "

# data_path_1 = 'C:/Users/g.vitale/Desktop/myStuff/ARCA/sample_data/RS32F0000002/True'
# data_path_2 = 'C:/Users/g.vitale/Desktop/myStuff/ARCA/sample_data/RS32F0000181/False'
# data_path_list = [data_path_1, data_path_2]
# options={'sensor'        : ['0'],
#           'currency'      : ['eu'],
#           'value'         : None, #['i','j'],
#           'emission'      : None, #['a'],
#           'orientation'   : None, #['1','2'],
#           'out_stats'     : True,
#                                 }
# ds = DatasetARCAtorch(data_path_list=data_path_list, 
#                       is_gt=True, 
#                       options=options,
#                       transform=None)
# dsi = iter(ds)
# item = next(dsi)
