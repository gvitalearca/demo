# -*- coding: utf-8 -*-
"""
Created on Wed Mar 24 17:17:45 2021

@author: g.vitale
"""

import os

import numpy as np

import torch
import torch.nn as nn
import torch.nn.parallel
import torch.optim as optim
import torch.utils.data
import torchvision.utils as vutils
import torchvision.models as vmodels

import matplotlib.pyplot as plt

from datasetArcaTorch import DatasetARCAtorch





" ---------- User Defined Parameters ---------------------------------------- "

" use of the GPU "
cuda = False

" image size "
# size = (256,256)

" save path "
save_path = '../data'

" initial weight pth file "
pretrained_checkpt = ''
saved_checkpt = ''
pretrain_torch = True

" numerical parameters "
lr0 = 0.01 
lr_ep = 5
mom = 0.9
w_decay = 0.0001
max_epoch = 21

" train only the last layer "
only_last = False

" batch size "
batch = 16

" classes of interest "
class_nms = ['value', 'orientation', 'emission']

" dataset options "
opt = {  'sensor'        : ('0','rf'),
         'currency'      : ['eu'],
         'value'         : None, #['i','j'],
         'emission'      : None, #['a'],
         'orientation'   : None, #['1','2'],
         'out_stats'     : True,
                               }

" user input "
train_path = 'C:/Users/g.vitale/Desktop/myStuff/ARCA/sample_data/RS32F0000181/False'
train_path_list = [train_path] 
test_path_1 = 'C:/Users/g.vitale/Desktop/myStuff/ARCA/sample_data/RS32F0000002/False'
test_path_2 = 'C:/Users/g.vitale/Desktop/myStuff/ARCA/sample_data/RS32G0000033/False'
test_path_list = [test_path_1, test_path_2] 

" --------------------------------------------------------------------------- "



" ----- Auxiliary Functions ------------------------------------------------- "

" Learning Rate Adjustment for training "
def adjust_learning_rate(optimizer, lr0, epoch, decay = 0.1, every_n=30):    
    lr = lr0 * (decay ** (epoch // every_n))
    for param_group in optimizer.param_groups:
        param_group['lr'] = lr

" --------------------------------------------------------------------------- "



" ----- Dataset Loading and Classes Definition ------------------------------ "

" datasets "
ds_train = DatasetARCAtorch(data_path_list=train_path_list, 
                      is_gt=True, 
                      options=opt.copy(),
                      transform='std')

ds_test = DatasetARCAtorch(data_path_list=test_path_list, 
                      is_gt=True, 
                      options=opt.copy(),
                      transform=None)


" classes definition "
ds_train.set_classes(class_nms)
ds_test.set_classes(class_nms)
n_class = len(ds_train.class2idx)

" --------------------------------------------------------------------------- "



" ----- Pytorch Data Processing --------------------------------------------- "

" device (cpu or gpu) where to run the code "
device = torch.device("cuda:0" if (torch.cuda.is_available() and cuda) else "cpu")

" pytorch dataloader (train) "
dload_train = torch.utils.data.DataLoader(ds_train, batch_size=batch,
                                          shuffle=True, num_workers=0)

" pytorch dataloader (test) "
dload_test = torch.utils.data.DataLoader(ds_test, batch_size=batch,
                                          shuffle=True, num_workers=0)

" visualization "
img_batch, _ = next(iter(dload_train))
plt.figure(figsize=(8,8))
plt.axis("off")
plt.title("Training Images")
plt.imshow(np.transpose(vutils.make_grid(img_batch.to(device)[:64], 
                                         padding=2, normalize=True).cpu(),
                        (1,2,0)))
plt.pause(3)

" --------------------------------------------------------------------------- "



" ----- Model Initialization ------------------------------------------------ "

" torch pretrained model (on imagenet) "
model = vmodels.resnet18(pretrained=pretrain_torch)

if not pretrain_torch: 
    if not pretrained_checkpt == '':
        " possible other pre-training "
        checkpt = torch.load(pretrained_checkpt)
        state_dict = {str.replace(k,'module.',''): v for k,v in checkpt['state_dict'].items()}
        model.load_state_dict(state_dict)
    elif not saved_checkpt == '':
        " possibly re-start from a saved checkpt "
        model.load_state_dict(torch.load(saved_checkpt))
    
" adjust: i) the output for our nb of classes; ii) the input for grayscale "
model.conv1 = torch.nn.Conv1d(1, 64, (7, 7), (2, 2), (3, 3), bias=False)
num_ftrs = model.fc.in_features
model.fc = nn.Linear(num_ftrs, n_class)

" set numerical optimization parameters "
if only_last == True:
    optimizer = optim.SGD(model.fc.parameters(), 
                          lr=lr0, 
                          momentum=mom, 
                          weight_decay=w_decay)
else:
    optimizer = optim.SGD(filter( lambda p: p.requires_grad, model.parameters() ), 
                          lr=lr0, 
                          momentum=mom, 
                          weight_decay=w_decay)
    
" --------------------------------------------------------------------------- "



" ----- Train the Net ------------------------------------------------------- "

" loop on epoch "
for epoch in range(0, max_epoch):

    " loop on the dataset "    
    model.to(device)	
    model.train()
    for batch_idx, item in enumerate(dload_train):
    		
        " get data "        
        data, target = [x.to(device) for x in item]
    
        " initialize gradient and evaluate model on data "
        optimizer.zero_grad()	
        output = model(data)    
        if type(output) == tuple:
            output, _ = output
    		
        " calc loss and make a gradient step "
        discr = nn.CrossEntropyLoss() 
        loss = discr(output, target)        
        loss.backward()
        optimizer.step()
    
        " output data "
        if batch_idx % 10 == 0:
            print('Train Epoch: {} [{}/{} ({:.0f}%)]\tLoss: {:.6f}'.format(
                		epoch, batch_idx, len(dload_train),
                		100. * batch_idx / len(dload_train), 
                        loss.item() ))
            
    " learning rate adjustment"
    adjust_learning_rate(optimizer, lr0, epoch, lr_ep)
    	
    " save the model each epoch --  model filename "	
    mfn = 'resnet18'+'_batch='+str(batch)+'_last='+str(only_last)+'_ep='+ str(epoch)
    torch.save(model.state_dict(), os.path.join(save_path, mfn))

" --------------------------------------------------------------------------- "



" ----- Test a checkpoint --------------------------------------------------- "

" load model "
epoch = 20
mfn = 'resnet18'+'_batch='+str(batch)+'_last='+str(only_last)+'_ep='+ str(epoch)
model.load_state_dict(torch.load(os.path.join(save_path, mfn)))

" loop on the dataset "    
model.to(device)	
model.eval()
confusion_mat = np.zeros((n_class, n_class))
lst = []
correct = 0
for batch_idx, item in enumerate(dload_test):
    		
    " get data "        
    data, target = [x.to(device) for x in item]
       
    with torch.no_grad():
        output = model(data)
        if type(output) == tuple:
            output, _ = output
    	
        " get the model prediction "
        _, pred = torch.max(output.data, 1)
        is_correct = (pred == target.data)
        for k in range(len(is_correct)):
            if not is_correct[k]:
                plt.figure()
                plt.imshow(np.transpose(data[k].to(device),(1,2,0)))
                plt.pause(2)
                print( 'pred: ' , ds_test.idx2class[pred[k].item()], 
                       '\n gt: ', ds_test.idx2class[target[k].item()] )

        " update metrics "
        correct += torch.sum(is_correct)
        i = pred.cpu().numpy()
        j = target.data.cpu().numpy()	
        for k in range(len(i)):
            confusion_mat[i[k],j[k]] += 1
print(correct)
correct = correct.item()/len(ds_test)
print(correct)

" --------------------------------------------------------------------------- "
