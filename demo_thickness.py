# -*- coding: utf-8 -*-
"""
Created on Mon Mar 22 17:11:58 2021

@author: g.vitale
"""

from random import sample as rc

import numpy as np
import cv2 as cv
from sklearn import svm
from sklearn.metrics import confusion_matrix as cm
from sklearn.cluster import KMeans as km
from sklearn.cluster import DBSCAN as db
from sklearn.decomposition import PCA
from sklearn.manifold import Isomap, LocallyLinearEmbedding, SpectralEmbedding, MDS
# from sklearn.preprocessing import StandardScaler as scaler
from sklearn.preprocessing import MinMaxScaler as scaler

import matplotlib.pyplot as plt
import matplotlib.colors as mcolors
from mpl_toolkits.mplot3d import Axes3D

import time

from datasetArca_v01 import DatasetARCA




" ----- Dataset Loading and Classes Definition ------------------------------ "

" user input "
data_path_1 = 'C:/Users/g.vitale/Desktop/myStuff/ARCA/sample_data/RS32F0000002/True'
data_path_2 = 'C:/Users/g.vitale/Desktop/myStuff/ARCA/sample_data/RS32F0000181/False'
data_path_list = [data_path_1, data_path_2]

" dataset "
options={'sensor'        : ['s'],
         'currency'      : ['eu'],
         'value'         : None, #['i','j'],
         'emission'      : None, #['a'],
         'orientation'   : None, #['1','2'],
         'out_stats'     : False,
                               }
ds = DatasetARCA(data_path_list=data_path_list, is_gt=True, options=options)
dsi = iter(ds)


" loop on thickness data "
m_hst = np.array([])
for n, item in enumerate(dsi): 
    thd = item['data']['s']
    # plt.figure()
    # plt.imshow(thd)
    v = thd.flatten()
    th_hst = np.histogram(v, bins=255, density=False)[0]
    if m_hst.size>0:
        m_hst = np.vstack((m_hst, th_hst))
    else:
        m_hst = th_hst
m = np.mean(m_hst, axis=0)
plt.plot(m[:-30])
np.argmax(m[:-30])
# plt.hist(v, bins=10)



