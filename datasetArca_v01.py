# -*- coding: utf-8 -*-
"""
Created on Fri Mar 12 17:02:30 2021

@author: g.vitale
"""

import os
import glob

from random import shuffle
import pandas as pd
import cv2 as cv



class DatasetARCA(object):
    
    def __init__(self, 
                 data_path_list='',
                 is_gt=True,
                 options={'sensor'        : None,
                          'currency'      : None,
                          'value'         : None,
                          'emission'      : None,
                          'orientation'   : None,
                          'out_stats'     : True,
                          } ):
        
        " input members "
        self.path_lst = data_path_list
        self.is_gt = is_gt
        self.options = options
                                
        " load database into pandas "
        lst = []
        for path in self.path_lst:
            label = path.split('/')[-1]
            acqs = [a for a in os.listdir(path) if os.path.isdir(os.path.join(path,a))]
            if self.is_gt:
                lst += self._read_db_gt(label, path, acqs)
            else:
                lst += self._read_db_to_classify(label, path, acqs)
        self.db = pd.DataFrame(lst)
        
        " reader and sensors info "        
        self.readers = list(set(self.db['reader']))
        self.avail_sensors = list(set(self.db['sensor']))
        
        " filter database according to user options "
        sen = self.avail_sensors if self.options['sensor'] is None else self.options['sensor']
        cur = list(set(self.db['currency'])) if self.options['currency'] is None else self.options['currency']
        val = list(set(self.db['value'])) if self.options['value'] is None else self.options['value']
        emi = list(set(self.db['emission'])) if self.options['emission'] is None else self.options['emission']
        ort = list(set(self.db['orientation'])) if self.options['orientation'] is None else self.options['orientation']
        self.db = self.db[self.db['sensor'].isin(sen) & self.db['currency'].isin(cur) &
                          self.db['value'].isin(val) & self.db['emission'].isin(emi) &
                          self.db['orientation'].isin(ort)]
        
        " group and randomize database for getitem purpose "
        if self.is_gt:
            self.cols = ['currency', 'value', 'orientation', 'emission', 'name', 'genuine', 'reader']
        else:
            self.cols = ['name']
        self.db_grouped = self.db.groupby(self.cols)
        self.db_keys = list(self.db_grouped.groups.keys())
        shuffle(self.db_keys)
        
        " output overall statistics if asked "
        if self.options['out_stats'] and self.is_gt:
            self._db_stats()
    
    def __getitem__(self, idx):
        item = {}
        cl = self.db_grouped.get_group(self.db_keys[idx])
        item['data'] = self._open_dat(cl[['sensor', 'data']])
        if self.is_gt:
            item['info'] = cl[self.cols].iloc[0].to_dict()
        else:
            item['info'] = {}
        return item
   
    
    
    " ----- Private Methods ------------------------------------------------- "
    
    " gt database reading "
    def _read_db_gt(self, label, path, acqs):
        lst = []
        for b in acqs:
            cur = b.split('_')[0][:-3]
            val = b.split('_')[0][-3]
            emi = b.split('_')[0][-2]
            ort = b.split('_')[0][-1]
            sns = b.split('_')[1][0]
            bmp_paths = glob.glob(os.path.join(path, b, '*.bmp'))
            reader = path.split('/')[-2]
            for ptr in bmp_paths:
                # nam = ptr.split('\\')[-1].replace('.bmp','')
                nam = ptr                    
                ist = {'name'          : nam,
                       'sensor'        : sns,
                       'data'          : ptr,
                       'currency'      : cur.lower(),
                       'value'         : val.lower(),
                       'emission'      : emi.lower(),
                       'orientation'   : ort,
                       'genuine'       : label,
                       'reader'        : reader                                           
                 }
                lst.append(ist)
        return lst

    " unlabeled database reading "  
    def _read_db_to_classify(self, path, acqs):
        lst = []
        for b in acqs:
            sns = b.split('_')[1]
            bmp_paths = glob.glob(os.path.join(path, b, '*.bmp'))
            reader = path.split('/')[-2]
            for ptr in bmp_paths:
                nam = ptr.split('\\')[-1].replace('.bmp','')                    
                ist = {'name'          : nam,
                       'sensor'        : sns,
                       'data'          : ptr,
                       'reader'        : reader                                                               
                 }
            lst.append(ist)
        return lst
    
    " open the bmp data the sensors "
    def _open_dat(self, ptrs):
        item = {}
        for avsns in list(set(self.db['sensor'])):
            ptr = ptrs[ptrs['sensor'] == avsns]['data'].iloc[0]
            item[avsns] = cv.imread(ptr,0)
            if avsns == '0':
                pxl_ch = item[avsns].shape[1]//7
                item['rf'] = item[avsns][:, 0*pxl_ch:1*pxl_ch]
                item['gf'] = item[avsns][:, 1*pxl_ch:2*pxl_ch]
                item['if'] = item[avsns][:, 2*pxl_ch:3*pxl_ch]
                item['rr'] = item[avsns][:, 3*pxl_ch:4*pxl_ch]
                item['gr'] = item[avsns][:, 4*pxl_ch:5*pxl_ch]
                item['ir'] = item[avsns][:, 5*pxl_ch:6*pxl_ch]
                item['tr'] = item[avsns][:, 6*pxl_ch:7*pxl_ch]
                item.pop('0')                                
        return item
     
    " basic database statistics "
    def _db_stats(self):
        print('--------------------------------------------------------------')
        print('Loaded Database at:')
        print(self.path_lst)
        print('--------------------------------------------------------------')
        print('Loaded Database fields:')
        print(self.db.columns)
        print('--------------------------------------------------------------')
        print('Available/Chosen sensors:')
        print(self.avail_sensors, ' / ', self.options['sensor'])
        print('--------------------------------------------------------------')
        print('Readers:')
        print(self.readers)        
        print('--------------------------------------------------------------')
        cols = ['currency', 'genuine', 'value', 'emission', 'orientation']
        cumc = []
        for c in cols:
            cumc.append(c) 
            db_by = self.db.groupby(cumc)
            keys = list(db_by.groups.keys())
            for k in keys:
                ln = len(db_by.get_group(k))
                print(ln, 'istance with', cumc, ' = ', k)            
        print('--------------------------------------------------------------')        



    " ----- Public Methods -------------------------------------------------- "
    # TODO
    # " database writing "
    # def write_db(self):
    #     if self.db.empty:
    #         return 0
    
    # TODO
    # pytorch dataset 'compatibility'










# " ----- TEST ---------------------------------------------------------------- "

# data_path_1 = 'C:/Users/g.vitale/Desktop/myStuff/ARCA/sample_data/RS32F0000002/True'
# data_path_2 = 'C:/Users/g.vitale/Desktop/myStuff/ARCA/sample_data/RS32F0000181/False'
# data_path_list = [data_path_1, data_path_2]

# ds = DatasetARCA(data_path_list=data_path_list, is_gt=True)
# dsi = iter(ds)
# item = next(dsi)
