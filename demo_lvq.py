# -*- coding: utf-8 -*-
"""
Created on Mon Mar 15 17:11:46 2021

@author: g.vitale
"""

import numpy as np
import cv2 as cv
# from glvq import GlvqModel as glvq
# from sklearn_lvq import glvq
# from neupy import algorithms
from sklearn import svm
from sklearn.metrics import confusion_matrix as cm
from sklearn.cluster import KMeans as km

import matplotlib.pyplot as plt
import matplotlib.colors as mcolors
from mpl_toolkits.mplot3d import Axes3D

import time

from datasetArca_v01 import DatasetARCA



" user input "
data_path_1 = 'C:/Users/g.vitale/Desktop/myStuff/ARCA/sample_data/RS32F0000002/True'
data_path_2 = 'C:/Users/g.vitale/Desktop/myStuff/ARCA/sample_data/RS32F0000181/False'
data_path_list = [data_path_1, data_path_2]

" dataset "
options={'sensor'        : ['0'],
         'currency'      : ['eu'],
         'value'         : None, #['j','e'],
         'emission'      : None, #['a'],
         'orientation'   : None, #['1'],
         'out_stats'     : True,
                               }
ds = DatasetARCA(data_path_list=data_path_list, is_gt=True, options=options)
dsi = iter(ds)

" classes "
class_nms = ['value']
keys = list(ds.db.groupby(class_nms).groups.keys())
class_ids = dict(zip(keys, range(len(keys))))
class_ids_inv = dict(zip( range(len(keys)), keys))

" feature extraction "
winSize =  (128,64) #(64,64) #(64,128)
blockSize = (32,32) #(16,16)
blockStride = (32,32) #(16,16) #(8,8)
cellSize = (32,32) #(16,16) #(8,8)
nbins = 9
derivAperture = 1
winSigma = 4.
histogramNormType = 0
L2HysThreshold = 2.0e-01
gammaCorrection = 0
nlevels = 64

hog = cv.HOGDescriptor(winSize, blockSize, blockStride, 
                        cellSize, nbins, derivAperture, winSigma,
                        histogramNormType, L2HysThreshold, 
                        gammaCorrection, nlevels)

winStride = (64,32) #(32,32) #(8,8)
padding = (8,8)

feats = []
label = []
infos = []
p = 20
for n, item in enumerate(dsi):    
    img = item['data']['rf']
    img = img[p:img.shape[0]-p, p:img.shape[1]-p]
    class_id = tuple(item['info'][class_nms])
    class_id = class_id if len(class_id)>1 else class_id[0]
    label.append(class_ids[class_id])
    infos.append(item['info'])
    fea = hog.compute(img, winStride, padding).squeeze()
    feats.append(fea)
feats = np.array(feats)
label = np.array(label)

" normalize data "
from sklearn.preprocessing import StandardScaler as scaler
# from sklearn.preprocessing import MinMaxScaler as scaler
feats = scaler().fit_transform(feats)


# " lvq training "
# model = glvq.GlvqModel()
# out = model.fit(feats, label)
# print('GLVQ score: ', out.score(feats, label)

# n_inp = fea.size
# n_cls = len(keys)
# lvqnet = algorithms.LVQ(n_inputs=n_inp, 
#                         n_classes=n_cls,
#                         verbose=True,
#                         show_epoch=20,
#                         shuffle_data=True,
#                         step=0.0001,
#                         # n_updates_to_stepdrop=150 * 100
#                         )
# lvqnet.train(feats, label, epochs=100)
# c = lvqnet.predict(feats)

" SVM multi "
clf = svm.SVC()
clf.fit(feats, label)
c = clf.predict(feats)

" output "
correct = label == c
not_corr_gt = [e for e,k in zip(infos, correct) if not k]
not_corr_pr = [class_ids_inv[l] for l,k in zip(c, correct) if not k]
z = list(zip(not_corr_pr, not_corr_gt))

print(cm(label, c))