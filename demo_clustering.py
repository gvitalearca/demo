# -*- coding: utf-8 -*-
"""
Created on Wed Mar 17 08:59:35 2021

@author: g.vitale
"""

from random import sample as rc

import numpy as np
import cv2 as cv
from sklearn import svm
from sklearn.metrics import confusion_matrix as cm
from sklearn.cluster import KMeans as km
from sklearn.cluster import DBSCAN as db
from sklearn.decomposition import PCA
from sklearn.manifold import Isomap, LocallyLinearEmbedding, SpectralEmbedding, MDS
# from sklearn.preprocessing import StandardScaler as scaler
from sklearn.preprocessing import MinMaxScaler as scaler

import matplotlib.pyplot as plt
import matplotlib.colors as mcolors
from mpl_toolkits.mplot3d import Axes3D

import time

from datasetArca_v01 import DatasetARCA



" ----- Dataset Loading and Classes Definition ------------------------------ "

" user input "
data_path_1 = 'C:/Users/g.vitale/Desktop/myStuff/ARCA/sample_data/RS32F0000002/True'
data_path_2 = 'C:/Users/g.vitale/Desktop/myStuff/ARCA/sample_data/RS32F0000181/False'
data_path_list = [data_path_1, data_path_2]

" dataset "
options={'sensor'        : ['0'],
         'currency'      : ['eu'],
         'value'         : None, #['i','j'],
         'emission'      : None, #['a'],
         'orientation'   : None, #['1','2'],
         'out_stats'     : False,
                               }
ds = DatasetARCA(data_path_list=data_path_list, is_gt=True, options=options)
dsi = iter(ds)

" classes definition "
class_nms = ['value']
keys = list(ds.db.groupby(class_nms).groups.keys())
class_ids = dict(zip(keys, range(len(keys))))
class_ids_inv = dict(zip( range(len(keys)), keys))




" ----- process data -------------------------------------------------------- "

" HOG features parameters "
winSize =  (64,64) #(64,128)
blockSize = (32,32) #(16,16)
blockStride = (32,32) #(8,8)
cellSize = (32,32) #(16,16) #(8,8)
nbins = 7
derivAperture = 1
winSigma = 4.
histogramNormType = 0
L2HysThreshold = 2.0e-01
gammaCorrection = 0
nlevels = 64

winStride = (8,8)
padding = (2,2)

" init HOG extractor "
hog = cv.HOGDescriptor(winSize, blockSize, blockStride, 
                        cellSize, nbins, derivAperture, winSigma,
                        histogramNormType, L2HysThreshold, 
                        gammaCorrection, nlevels)


" auxiliary image cropping "
def crop_img(img, p=20):
    return img[p:img.shape[0]-p, p:img.shape[1]-p]

" auxiliary function to treat multiple feature arrays "
def symmetrizer(x):
    # sym_x = np.max(x, axis=0)
    # sym_x = np.mean(x, axis=0)
    sym_x = np.reshape(x,(x.size))
    return sym_x

" flip-symmetric features "
def sym_hist_feat(im):
    th = 5
    bins = 10
    feat = np.zeros((im.shape[1],bins))
    for i in range(im.shape[1]-th-1):
        clm = im[:,i:i+th]
        feat[i,:] = np.histogram(clm, bins=bins, density=True)[0]
    return np.reshape(feat,(feat.size))


" loop on dataset computing features "
feats = []
label = []
k_opt_sns = ['rf', 'gf', 'rr', 'gr']
for n, item in enumerate(dsi):    
    class_id = [item['info'][c] for c in class_nms]
    class_id = class_id if len(class_id)>1 else class_id[0]
    label.append(class_ids[class_id])
    loc_feat = np.array([])
    for k in k_opt_sns:
        img = item['data'][k]
        img = crop_img(img)
        fea = hog.compute(img, winStride, padding).squeeze()
        # fea = np.array([])
        # clr = np.histogram(img, bins=10, density=True)[0]
        # fea = np.append(fea, clr)
        # fea = sym_hist_feat(img)
        if loc_feat.size>0:
            loc_feat = np.vstack((loc_feat, fea))
        else:
            loc_feat = fea
        # fea = hog.compute(np.flip(img), winStride, padding).squeeze()
        # loc_feat = np.vstack((loc_feat, fea))
    sym_feat = symmetrizer(loc_feat)
    feats.append(sym_feat)

" scale/normalize features "
feats = np.array(feats)
# feats = scaler().fit_transform(feats)


" ----- plotting and visualization ------------------------------------------ "
#TODO: other manifold dim reduction

" plot colors by classes "
class_col = rc(list(mcolors.TABLEAU_COLORS.values()), len(keys))
col = [class_col[l] for l in label]

" 3d compression "
pca = PCA(n_components=3)
iso = Isomap(n_components=3)
lle = LocallyLinearEmbedding(n_components=3)
spe = SpectralEmbedding(n_components=3)
mds = MDS(n_components=3)
dim_red = pca
red_feats = dim_red.fit_transform(feats)

" 3d plot "
fig = plt.figure(1, figsize=(4, 3))
plt.clf()
ax = Axes3D(fig, rect=[0, 0, .95, 1], elev=48, azim=134)
plt.cla()
ax.scatter(red_feats[:, 0], red_feats[:, 1], red_feats[:, 2], 
           c=col,           
           cmap=plt.cm.nipy_spectral,
           edgecolor='k')
ax.w_xaxis.set_ticklabels([])
ax.w_yaxis.set_ticklabels([])
ax.w_zaxis.set_ticklabels([])
plt.show()

" 2d plot "
plt.figure(1, figsize=(4, 3))
sc = plt.scatter(red_feats[:, 0], red_feats[:, 1], 
           c=col,           
            cmap=plt.cm.nipy_spectral,
            edgecolor='k')
plt.show()











"""
" ----- Dataset Loading and Classes Definition ------------------------------ "

" user input "
data_path_1 = 'C:/Users/g.vitale/Desktop/myStuff/ARCA/sample_data/RS32F0000002/True'
data_path_2 = 'C:/Users/g.vitale/Desktop/myStuff/ARCA/sample_data/RS32F0000181/False'
data_path_list = [data_path_1, data_path_2]

" dataset "
options={'sensor'        : ['0'],
         'currency'      : ['eu'],
         'value'         : None, #['j','e'],
         'emission'      : None, #['a'],
         'orientation'   : None, #['1'],
         'out_stats'     : False #True
                               }
ds = DatasetARCA(data_path_list=data_path_list, is_gt=True, options=options)
dsi = iter(ds)

" classes "
class_nms = ['value']
keys = list(ds.db.groupby(class_nms).groups.keys())
class_ids = dict(zip(keys, range(len(keys))))
class_ids_inv = dict(zip( range(len(keys)), keys))



" ----- Feature Extraction -------------------------------------------------- "

" HOG features parameters "
winSize =  (64,64) #(64,128)
blockSize = (32,32) #(16,16)
blockStride = (32,32) #(8,8)
cellSize = (32,32) #(16,16) #(8,8)
nbins = 7
derivAperture = 1
winSigma = 4.
histogramNormType = 0
L2HysThreshold = 2.0e-01
gammaCorrection = 0
nlevels = 64

winStride = (32,32) #(8,8)
padding = (8,8)

" init HOG object "
hog = cv.HOGDescriptor(winSize, blockSize, blockStride, 
                        cellSize, nbins, derivAperture, winSigma,
                        histogramNormType, L2HysThreshold, 
                        gammaCorrection, nlevels)

# " auxiliary function to treat multiple feature arrays "
# def symmetrizer(x):
#     if type(x)==list:
#         return 0
#     else:
#         raise Warning('symmetrizer')

" compute features for all the input images "
p = 20
feats = []
label = []
infos = []
for n, item in enumerate(dsi):    
    img = item['data']['rf']
    img = img[p:img.shape[0]-p, p:img.shape[1]-p]
    # label.append(item['info']['value'])
    class_id = tuple(item['info'][class_nms])
    class_id = class_id if len(class_id)>1 else class_id[0]
    label.append(class_ids[class_id])
    fea = hog.compute(img, winStride, padding).squeeze()
    # fea = hog.compute(img, winStride, padding, locations).squeeze()
    # fea = hog.compute(img).squeeze()
    feats.append(fea)

# p=20
# feats = []
# label = []
# infos = []
# k_opt_sns = ['rf']# ['rf', 'gf', 'rr', 'gr']
# for n, item in enumerate(dsi):
#     loc_feat = []
#     class_id = tuple(item['info'][class_nms])
#     class_id = class_id if len(class_id)>1 else class_id[0]
#     label.append(class_ids[class_id])
#     infos.append(item['info'])    
#     for k in k_opt_sns:
#         img = item['data'][k]
#         img = img[p:img.shape[0]-p, p:img.shape[1]-p]
#         # loc_feat.append(hog.compute(img, winStride, padding).squeeze()) 
#         loc_feat += list(hog.compute(img, winStride, padding).squeeze())
#     # sym_feat = symmetrizer(loc_feat)
#     sym_feat = loc_feat
#     feats.append(sym_feat)
    
feats = np.array(feats)
label = np.array(label)



" ----- PCA Compression and Visualization ----------------------------------- "

# " normalize data "
# feats = scaler().fit_transform(feats)

" PCA compression "
n_comp = 3
pca = PCA(n_components=n_comp)
pca.fit(feats)
pc_feats = pca.transform(feats)

" Plots "
fig = plt.figure(1, figsize=(4, 3))
plt.clf()
ax = Axes3D(fig, rect=[0, 0, .95, 1], elev=48, azim=134)
plt.cla()
class_col = rc(list(mcolors.TABLEAU_COLORS.values()), len(keys))
col = [class_col[l] for l in label]
ax.scatter(feats[:, 0], feats[:, 1], feats[:, 2], 
           c=col,           
           cmap=plt.cm.nipy_spectral,
           edgecolor='k')
ax.w_xaxis.set_ticklabels([])
ax.w_yaxis.set_ticklabels([])
ax.w_zaxis.set_ticklabels([])
plt.show()



# " k-means clustering "
# n_cls = len(keys)
# kmeans = km(init="random", #"k-means++", 
#             n_clusters=n_cls, 
#             n_init=10,
#             random_state=0)
# kmeans.fit(pc_feats)
# # kmeans.fit(feats)
# clusters = kmeans.predict(pc_feats)

# " dbscan "
# dbscan = db(eps=0.8, min_samples=5)
# dbscan.fit(pc_feats)
# lbl = dbscan.labels_
# ncl = len(set(dbscan.labels_))
# nns = list(lbl).count(-1)
# print(lbl)
# print(ncl)
# print(nns)

# " spectral "
# from sklearn.cluster import SpectralClustering
# assign = 'kmeans' #'discretize'
# n_cls = len(keys)
# n_cmp = 2*n_cls
# aff = 'nearest_neighbors' #'precomputed'
# sc = SpectralClustering(n_cls, 
#                         affinity=aff, 
#                         n_neighbors=20,
#                         n_components=n_cmp,
#                         n_init=100,
#                         assign_labels=assign)
# clusters = sc.fit_predict(pc_feats)  

# " misfit calculation "
# corresp = set(zip(label,clusters))
# print(len(corresp) - len(keys))

"""